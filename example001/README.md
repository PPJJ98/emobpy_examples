This is a basic script in jupyter notebook to access to time-series dataframes from the DataBase class.

emobpy version: 0.6.2

Note: Make sure you have installed jupyter in the same conda environment as emobpy.
